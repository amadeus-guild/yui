#![feature(vec_remove_item)]
#[macro_use]
extern crate text_io;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;

extern crate ctrlc;

mod utils;
mod modules;

use std::{fs};

fn main() -> std::io::Result<()> {
    println!("Salut je suis Yui alias Yui-MHCP001,\nJe suis ici pour vous aider dans votre quête de L'aincrad");
    
    fs::create_dir_all("./heart/data")?;
    modules::markets::load_markets();
    modules::players::load_players();
    modules::guilds::load_guilds();
    modules::monsters::load_monsters();

    utils::item::load_items();
    utils::house::load_houses();
    utils::location::load_locations();

    ctrlc::set_handler(move || {
        save_all();
    }).expect("Error setting Ctrl-C handler");

    loop {
        println!("==================");
        println!(
            "1>Items\n2>Magasins\n3>Guildes\n4>Joueurs\n5>Monstres\n6>Locations\n7>Maisons\n8>Exit");
        let cat:i32 = read!();

        match cat {
            1 => utils::item::item(),
            2 => modules::markets::markets(),
            3 => modules::guilds::guilds(),
            4 => modules::players::player(),
            5 => modules::monsters::monsters(),
            6 => utils::location::location(),
            7 => utils::house::house(),
            _ => {
                save_all();
                println!("A plus tard :)");
                break;
            }
        } 
    }

    Ok(())
}

fn save_all() {
    modules::markets::save_markets();
    modules::players::save_players();
    modules::guilds::save_guilds();
    modules::monsters::save_monsters();

    utils::item::save_items();
    utils::location::save_locations();
    utils::house::save_houses();

    println!("All config saved.");
}