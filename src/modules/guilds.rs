extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::str::FromStr;
use std::sync::Mutex;

use utils::house;

#[derive(Serialize, Deserialize,Clone, Debug)]
pub enum Type {
    Verte,
    Orange,
    Undefined
}

impl FromStr for Type {
    type Err = ();

    fn from_str(s: &str) -> Result<Type, ()> {
        match s {
            "Verte" => Ok(Type::Verte),
            "Orange" => Ok(Type::Orange),
            _ => Ok(Type::Undefined),
        }
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Guild {
    name:String,
    guild_type:Type,
    power:u32,
    description:String,
    players:Vec<String>,
    house:Option<String>,
}

impl Guild {
    fn new_guild(name:String, guild_type:String, power:u32,description:String, house:Option<String>) -> Guild {
        Guild {
            name,
            guild_type:Type::from_str(&guild_type).unwrap(),
            power,
            description,
            players:Vec::new(),
            house,
        }
    }

    fn get_housename(&self) -> String {
        match self.house {
            Some(_) => format!("Leurs maison est appelée {}", self.house.clone().unwrap()),
            None => String::from("La guilde n'a pas de maison")
        } 
    }
}


lazy_static! {
    static ref guilds_list: Mutex<HashMap<String, Guild>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn guilds() {
    println!("==================");
    println!("1>Créer une Guilde\n2>Liste des Guildes\n3>Voir une Guilde\n4>Supprimer une Guilde\n5>Ajouter un Joueur\n6>Retirer un joueur\n7>Sauvegarder les Guildes\n8>Retour en arrière.");

    let cat:u32 = read!("{}\n");

    match cat {
        1 => create_guild(),
        2 => list_guilds(),
        3 => see_guild(),
        4 => delete_guild(),
        5 => add_player(),
        6 => remove_player(),
        7 => save_guilds(),
        _ => return,
    }
}

fn create_guild() {
    println!("Entrez le nom de la guilde.");
    let name:String = read!("{}\n");

    println!("Entrez le type de la guilde : Orange, Verte, Indefinie");
    let guild_type:String = read!("{}\n");

    println!("Entrez une puissance approximative");
    let power:u32 = read!("{}\n");

    println!("Entrez la description de la guilde.");
    let description:String = read!("{}\n");

    let house:Option<String> = house::see_house();

    let guild:Guild = Guild::new_guild(name, guild_type, power, description, house);

    println!("La Guilde {} a bien été crée.", &guild.name);
    guilds_list.lock().unwrap().insert(guild.name.clone(), guild);
}

fn list_guilds() {
    println!("Voici la liste des Guildes.");
    let i = guilds_list.lock().unwrap();
    for (name,guild) in i.iter() {
        println!("{}, {} Joueurs, {} de puissance", name, &guild.players.len(), &guild.power);
    }
}

pub fn see_guild() {
    println!("Entrez le nom de la guilde.");
    let name:String = read!("{}\n");

    let a = guilds_list.lock().unwrap();

    if a.contains_key(&name) {
        let i = &a.get(&name).unwrap();

        println!("La guilde {} est une guilde {:?}, elle est constituée de {} Membres, elle a une puissance de {}", &i.name, &i.guild_type, &i.players.len(), &i.power);
        println!("{}", &i.get_housename());

        println!("Voulez vous afficher la liste des joueurs ?\n1>Oui\n2>Non");
        let choice:u32 = read!("{}\n");

        if choice == 1 {
            println!("Joueurs :");
            for player in &i.players {
                println!("{}", player);
            }
        }
    }
}

fn delete_guild() {
    println!("Entrez le nom la guilde a supprimer");
    let name:String = read!("{}\n");

    let mut a = guilds_list.lock().unwrap();

    if a.contains_key(&name) {
        a.remove(&name);
    } else {
        println!("Guilde introuvable.")
    }
}

fn add_player() {
    println!("Entrez le nom de la guilde au quel ajouter un joueur.");
    let guild_name:String = read!("{}\n");

    println!("Entrez le nom du joueur a ajouter.");
    let name:String = read!("{}\n");

    let mut guilds = guilds_list.lock().unwrap();

    if !guilds.contains_key(&guild_name) {
        println!("Cette guilde n'existe pas.");
        return;
    }
    let mut guild:Guild = guilds.get(&guild_name).unwrap().clone();

    guild.players.push(name.clone());

    println!("Le Joueur : {} a bien été ajouté de la guilde {}.", &name, &guild.name);
    guilds.insert(guild.name.clone(), guild);
}

fn remove_player() {
    println!("Entrez le nom de la guilde au quel enlever un joueur.");
    let guild_name:String = read!("{}\n");

    println!("Entrez le nom du joueur à enlever.");
    let name:String = read!("{}\n");

    let mut guilds = guilds_list.lock().unwrap();
    if !guilds.contains_key(&guild_name) {
        println!("Cette guilde n'existe pas.");
        return;
        }
    let mut guild:Guild = guilds.get(&guild_name).unwrap().clone();

    if guild.players.contains(&name) {
        guild.players.remove_item(&name);

        println!("Le Joueur : {} a bien été enlevé de la guilde {}.", &name, &guild.name);
        guilds.insert(guild.name.clone(), guild);
    } else {
        println!("Ce joueur n'est pas dans cette guilde.");
    }
}

pub fn save_guilds() {
    let i = guilds_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/guilds.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/guilds.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_guilds() {
        // Create a path to the desired file
    let path = Path::new("./heart/data/guilds.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Guild> = serde_json::from_str(&s).unwrap();

            for (name, guild) in &map {
                guilds_list.lock().unwrap().insert(name.clone(), guild.clone());
            }
        },
    }
}