extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::sync::Mutex;

use utils::{location, item};

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct TradeItem {
    name:String,
    number:u32,
}

impl TradeItem {
    fn create_item(name:String) -> TradeItem {

        println!("Entrez le nombre de {} utilisée. (Entre 1 et 64)", &name);
        let number:u32 = read!("{}\n");
        TradeItem {name, number}
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Trade {
    item1:TradeItem,
    item2:Option<TradeItem>,
    reward:TradeItem,
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Market {
    name:String,
    level:u32,
    loc:String,
    description:String,
    trades:Vec<Trade>,
}

impl Market {
    fn add_trade(&mut self, trade:Trade) {
        self.trades.push(trade);
    }
}

lazy_static! {
    static ref market_list: Mutex<HashMap<String, Market>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn markets() {
    println!("1>Créer un magasin\n2>Liste des magasins\n3>Voir un magasin\n4>Supprimer un magasin\n5>Ajouter un trade à un magasin\n6>Enlever un trade d'un magasin\n7>Sauvegarder les magasins\n8>Quitter");

    let cat:u32 = read!("{}\n");

    match cat {
        1 => create_market(),
        2 => list_markets(),
        3 => see_market(),
        4 => delete_market(),
        5 => add_to_market(),
        6 => remove_from_market(),
        7 => save_markets(),
        _ => return,
    }
}

fn create_market() {
    println!("Nom du magasin.");
    let name:String = read!("{}\n");

    println!("Niveau du magasin.");
    let level:u32 = read!("{}\n");

    println!("Lieu du magasin.");
    let loca:Option<String> = location::see_loc();

    let loc = match loca {
        Some(_) => loca.unwrap(),
        None => {
            println!("Location invalide, veuillez la créer avant.");
            return
        },
    };

    println!("Entrez une description.");
    let description:String = read!("{}\n");
    
    println!("Entrez le nom de l'item reward.");
    let _item_name:Option<String> = item::see_item();
    let item_name = match _item_name {
        Some(_) => _item_name.unwrap(),
        None => {
            println!("Item introuvable, veuillez le créer avant.");
            return
        },
    };

    let reward:TradeItem = TradeItem::create_item(item_name);

    println!("Entrez le nom du premier item à utiliser.");
    let _item_name:Option<String> = item::see_item();
    let item_name = match _item_name {
        Some(_) => _item_name.unwrap(),
        None => {
            println!("Item introuvable, veuillez le créer avant.");
            return
        },
    };

    let item1:TradeItem = TradeItem::create_item(item_name);

    println!("Utiliser un deuxième item ?\n1>Oui\n2>Non");
    let choice:u32 = read!("{}\n");

    let item2:Option<TradeItem> = match choice {
        1 => {
            println!("Entrez le nom du premier item à utiliser.");
            let _item_name:Option<String> = item::see_item();
            let item_name = match _item_name {
                Some(_) => _item_name.unwrap(),
                None => {
                    println!("Item introuvable, veuillez le créer avant.");
                    return 
                },
            };

            let item2:TradeItem = TradeItem::create_item(item_name);
            Some(item2)
        },

        _ => {
            None
        },
    };
    let trade:Trade = Trade {item1, item2, reward};

    let trades = vec![trade];

    let market:Market = Market { name, level, loc, description, trades};

    println!("Le magasin : {} a bien été crée.", &market.name);
    market_list.lock().unwrap().insert(market.name.clone(), market);
}

fn list_markets() {
    println!("Voici la liste des Magasins.");
    let i = market_list.lock().unwrap();
    for (name, market) in i.iter() {
        println!("{}, situé à {} et est pour niveau {}", name, &market.loc, &market.level);
    }
}

fn see_market() {
    println!("Entrez le nom du Magasin.");
    let name:String = read!("{}\n");

    let a = market_list.lock().unwrap();

    if a.contains_key(&name) {
        let market = &a.get(&name).unwrap();
        println!("{}, situé à {} et est pour niveau {}", name, &market.loc, &market.level);
        println!("Description: {}", &market.description);
        for trade in &market.trades {
            match &trade.item2 {
                Some(_) => {
                    println!("{} {} + {} {} = {} {}", &trade.item1.number, &trade.item1.name, &trade.clone().item2.unwrap().clone().number, &trade.clone().item2.unwrap().clone().name, &trade.reward.number, &trade.reward.name);
                },
                _ => {
                    println!("{} {} = {} {}", &trade.item1.number, &trade.item1.name, &trade.reward.number, &trade.reward.name);
                }
            }
        }
    }
}

fn add_to_market() {
    println!("Entrez le nom du magasin.");
    let name:String = read!("{}\n");

    let mut a = market_list.lock().unwrap();
    if a.contains_key(&name) {
        let mut market:Market = a.get(&name).unwrap().clone();
        
         println!("Entrez le nom de l'item reward.");
    let _item_name:Option<String> = item::see_item();
    let item_name = match _item_name {
        Some(_) => _item_name.unwrap(),
        None => {
            println!("Item introuvable, veuillez le créer avant.");
            return
        },
    };

    let reward:TradeItem = TradeItem::create_item(item_name);

    println!("Entrez le nom du premier item à utiliser.");
    let _item_name:Option<String> = item::see_item();
    let item_name = match _item_name {
        Some(_) => _item_name.unwrap(),
        None => {
            println!("Item introuvable, veuillez le créer avant.");
            return
        },
    };

    let item1:TradeItem = TradeItem::create_item(item_name);

    println!("Utiliser un deuxième item ?\n1>Oui\n2>Non");
    let choice:u32 = read!("{}\n");

    let item2:Option<TradeItem> = match choice {
        1 => {
            println!("Entrez le nom du premier item à utiliser.");
            let _item_name:Option<String> = item::see_item();
            let item_name = match _item_name {
                Some(_) => _item_name.unwrap(),
                None => {
                    println!("Item introuvable, veuillez le créer avant.");
                    return 
                },
            };

            let item2:TradeItem = TradeItem::create_item(item_name);
            Some(item2)
        },

        _ => {
            None
        },
    };
    let trade:Trade = Trade {item1, item2, reward};

        market.add_trade(trade);

        println!("Le magasin : {} a bien été éditée.", &market.name);
        a.insert(market.name.clone(), market);
    }
}

fn remove_from_market() {
    println!("Entrez le nom du magasin.");
    let name:String = read!("{}\n");

    let mut a = market_list.lock().unwrap();
    if a.contains_key(&name) {
        let mut market:Market = a.get(&name).unwrap().clone();
        let mut i = 0;
        for trade in &market.trades {
            match &trade.item2 {
                Some(_) => {
                    println!("{}> {} {} + {} {} = {} {}",&i, &trade.item1.number, &trade.item1.name, &trade.clone().item2.unwrap().clone().number, &trade.clone().item2.unwrap().clone().name, &trade.reward.number, &trade.reward.name);
                    i+=1;
                },
                _ => {
                    println!("{}> {} {} = {} {}",&i, &trade.item1.number, &trade.item1.name, &trade.reward.number, &trade.reward.name);
                    i+=1;
                }
            }
        }

        println!("Entrez le numéro du trade a supprimer.");
        let suppr:usize = read!("{}\n");
        market.trades.remove(suppr);

        println!("Le magasin : {} a bien été éditée.", &market.name);
        a.insert(market.name.clone(), market);
    }
}

fn delete_market() {
    println!("Entrez le nom du magasin à supprimer");
    let name:String = read!("{}\n");

    let mut a = market_list.lock().unwrap();

    if a.contains_key(&name) {
        a.remove(&name);
    } else {
        println!("Magasin introuvable.")
    }
}

pub fn save_markets() {
      let i = market_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/markets.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/markets.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_markets() {
        // Create a path to the desired file
    let path = Path::new("./heart/data/markets.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Market> = serde_json::from_str(&s).unwrap();

            for (name, market) in &map {
                market_list.lock().unwrap().insert(name.clone(), market.clone());
            }
        },
    }
}