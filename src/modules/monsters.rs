extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::str::FromStr;
use std::sync::Mutex;

use utils::{location,stats};
use utils::stats::Stats;

#[derive(Serialize, Deserialize,Clone, Debug)]
pub enum Type {
    Agressif,
    Neutre,
    Passif,
    Undefined,
}

impl FromStr for Type {
    type Err = ();

    fn from_str(s: &str) -> Result<Type, ()> {
        match s {
            "Agressif" => Ok(Type::Agressif),
            "Neutre" => Ok(Type::Neutre),
            "Passif" => Ok(Type::Passif),
            _ => Ok(Type::Undefined),
        }
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub enum MonsterType {
    Boss,
    SemiBoss,
    Monstre,
    Undefined,
}

impl FromStr for MonsterType {
    type Err = ();

    fn from_str(s: &str) -> Result<MonsterType, ()> {
        match s {
            "Boss" => Ok(MonsterType::Boss),
            "SemiBoss" => Ok(MonsterType::SemiBoss),
            "Monstre" => Ok(MonsterType::Monstre),
            _ => Ok(MonsterType::Undefined),
        }
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Monster {
    name:String,
    level: u32,
    atype:Type,
    monster_type:MonsterType,
    location:String,
    stats:stats::Stats,
    description:String,
    respawn_time:u32,
    strategie:String,
    drop:Vec<String>,
}

impl Monster {
    fn new_monster(name:String, level:u32, atype:String, monster_type:String, location:String, stats:Stats, description:String, respawn_time:u32, strategie:String) -> Monster {
        Monster {
            name,
            level,
            atype:Type::from_str(&atype).unwrap(),
            monster_type:MonsterType::from_str(&monster_type).unwrap(),
            location,
            stats,
            description,
            respawn_time,
            strategie,
            drop:Vec::new(),
        }
    }
}

lazy_static! {
    static ref monster_list: Mutex<HashMap<String, Monster>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn monsters() {
    println!("==================");
    println!("1>Créer un Monstre\n2>Liste des Monstres\n3>Voir un Monstre\n4>Supprimer un Monstre\n5>Sauvegarder les Monstres\n6>Retour en arrière.");

    let cat:u32 = read!("{}\n");

    match cat {
        1 => create_monster(),
        2 => list_monters(),
        3 => see_monster(),
        4 => delete_monster(),
        5 => save_monsters(),
        _ => return,
    }
}

fn create_monster() {
    println!("Entrez le nom du monstre.");
    let name:String = read!("{}\n");
    
    println!("Entrez le niveau du monstre.");
    let level:u32 = read!("{}\n");

    println!("Entrez le type [Agressif, Neutre, Passif]");
    let atype:String = read!("{}\n");

    println!("Entrez le type de monstre [Boss, Semi Boss, Monstre]");
    let monster_type:String = read!("{}\n");

    let loca:Option<String> = location::see_loc();

    let loc = match loca {
        Some(_) => loca.unwrap(),
        None => {
            println!("Location invalide, veuillez la créer avant.");
            return
        },
    };

    let stats:Stats = stats::create_stats();

    println!("Entrez une description.");
    let description:String = read!("{}\n");

    println!("Entrez le temps de respawn (En Minute).");
    let respawn_time:u32 = read!("{}\n");

    println!("Entrez la stratégie du monstre.");
    let strategie:String = read!("{}\n");

    let monster:Monster = Monster::new_monster(name, level, atype, monster_type, loc, stats, description, respawn_time, strategie);

    println!("Le Monstre {} a bien été crée.", &monster.name);
    monster_list.lock().unwrap().insert(monster.name.clone(), monster);
}

fn list_monters() {
    println!("Voici la liste des Monstres.");
    let i = monster_list.lock().unwrap();
    for (name, monster) in i.iter() {
        println!("{}, Niveau {}, Situé à {}", name, &monster.level, &monster.location);
    }
}

fn see_monster() {
    println!("Entrez le nom du monstre.");
    let name:String = read!("{}\n");

    let a = monster_list.lock().unwrap();

    if a.contains_key(&name) {
        let i = &a.get(&name).unwrap();

        println!("{} est un {:?}, il est {:?} et vit aux alentours de {}, il respawn toutes les {} minutes.", &i.name, &i.monster_type, &i.atype, &i.location, &i.respawn_time);
        println!("=========\nVie: {}\nAttaque : {},\nDefense: {}\nVitesse: {}\n=========",&i.stats.health, &i.stats.attack, &i.stats.defence, &i.stats.speed);
        println!("Description: \n {} \n Strategie: {}", &i.description, &i.strategie);
    }
}

pub fn delete_monster() {
    println!("Entrez le nom du monstre a supprimer");
    let name:String = read!("{}\n");

    let mut a = monster_list.lock().unwrap();

    if a.contains_key(&name) {
        a.remove(&name);
    } else {
        println!("Monstre introuvable.")
    }
}

pub fn save_monsters() {
    let i = monster_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/monster.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/monster.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_monsters() {
        // Create a path to the desired file
    let path = Path::new("./heart/data/monster.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Monster> = serde_json::from_str(&s).unwrap();

            for (name, monster) in &map {
                monster_list.lock().unwrap().insert(name.clone(), monster.clone());
            }
        },
    }
}