extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::str::FromStr;
use std::sync::Mutex;

use utils::house;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Ranks {
    Staff,
    Beater,
    BetaTesteur,
    Joueur,
    NoRank,
}

impl FromStr for Ranks {
    type Err = ();

    fn from_str(s: &str) -> Result<Ranks, ()> {
        match s {
            "Staff" => Ok(Ranks::Staff),
            "Beater" => Ok(Ranks::Beater),
            "BetaTesteur" => Ok(Ranks::BetaTesteur),
            "Joueur" => Ok(Ranks::Joueur),
            _ => Ok(Ranks::NoRank),
        }
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Player {
    name:String,
    level:u32,
    power:u32,
    description:String,
    rank:Ranks,
    house:Option<String>,
}

impl Player {
    fn new_player(name:String, level:u32, power:u32, description:String, rank:String, house:Option<String>) -> Player {
        Player{
            name,
            level,
            power,
            description,
            rank:Ranks::from_str(&rank).unwrap(),
            house,
        }
    }

    fn get_housename(&self) -> String {
        match self.house {
            Some(_) => format!("Sa maison est appelée {}", self.house.clone().unwrap()),
            None => String::from("Le joueur n'a pas de maison")
        } 
    }
}

lazy_static! {
    static ref player_list: Mutex<HashMap<String, Player>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn player() {
    println!("==================");
    println!("1>Créer un Joueurs\n2>Liste des Joueurs\n3>Voir un Joueur\n4>Supprimer un Joueur\n5>Sauvegarder les Joueurs\n6>Retour en arrière.");

    let cat:u32 = read!("{}\n");

    if cat == 1 {
        create_player();
    } else if cat == 2 {
        list_players();
    } else if cat == 3 {
        see_player();
    } else if cat == 4 {
        delete_player();
    } else if cat == 5 {
        save_players();
    }
}

fn create_player() {
    println!("Entrez le nom du joueur.");
    let name:String = read!("{}\n");

    println!("Entrez le Niveau du joueur.");
    let level:u32 = read!("{}\n");

    println!("Entrez la puissance approximative.");
    let power:u32 = read!("{}\n");

    println!("Entrez une petite description.");
    let description:String = read!("{}\n");

    println!("Entrez le grade du joueur : Staff, Beater, BetaTesteur, Joueur");
    let rank:String = read!("{}\n");

    println!("Entrez le nom de sa maison.");
    let house:Option<String> = house::see_house();

    let player:Player = Player::new_player(name,level,power,description,rank,house);

    println!("Le Joueur {} a bien été crée.", &player.name);
    player_list.lock().unwrap().insert(player.name.clone(), player);
}

fn list_players() {
    println!("Voici la liste des Joueurs.");
    let i = player_list.lock().unwrap();
    println!("{:?}", i.keys());
}

pub fn see_player() -> Option<String> {
    println!("Entrez le nom du joueur.");
    let name:String = read!("{}\n");

    let a = player_list.lock().unwrap();

    if a.contains_key(&name) {
        let i = &a.get(&name).unwrap();
        
        println!("Le joueurs {} est niveau {} a une puissance de {}, sont grade est {:?}", &i.name, &i.power, &i.level, &i.rank);
        println!("{}, description: {}", &i.get_housename(), &i.description);
        return Some(i.name.clone())
    }
    println!("Le joueur n'existe pas.");
    None
}

fn delete_player() {
    println!("Entrez le nom du joueur a supprimer");
    let name:String = read!("{}\n");

    let mut a = player_list.lock().unwrap();

    if a.contains_key(&name) {
        a.remove(&name);
    } else {
        println!("Joueur introuvable.")
    }
}

pub fn save_players() {
    let i = player_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/players.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/players.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_players() {
    // Create a path to the desired file
    let path = Path::new("./heart/data/players.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Player> = serde_json::from_str(&s).unwrap();

            for (name, player) in &map {
                player_list.lock().unwrap().insert(name.clone(), player.clone());
            }
        },
    }
}