extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::sync::Mutex;

use utils::location;

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct House {
    name:String,
    location:String,
    price:u32,
    description:String,
}

impl House {
    fn new_house(name:String, location:String, price:u32, description:String) -> House {
        House {name, location,price,description}
    }
}

lazy_static! {
    static ref house_list: Mutex<HashMap<String, House>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn house() {
    println!("==================");
    println!("1>Créer une maison\n2>Liste des maisons\n3>Voir une maison\n4>Sauvegarder les maisons\n5>Retour en arrière");

    let cat:i32 = read!();

    if cat == 1 {
        create_house();
    } else if cat == 2 {
        list_house();
    } else if cat == 3 {
        see_house();
    } else if cat == 4 {
        save_houses();
    }
}

fn create_house() {
    println!("Entrez le nom de la maison.");
    let name:String = read!("{}\n");

    let loca:Option<String> = location::see_loc();

    let loc = match loca {
        Some(_) => loca.unwrap(),
        None => {
            println!("Location invalide, veuillez la créer avant.");
            return
        },
    };

    println!("Entrez le prix de la maison.");
    let price:u32 = read!("{}\n");

    println!("Entrez la description de la maison.");
    let description:String = read!("{}\n");

    let house:House = House::new_house(name, loc, price, description);

    println!("La maison {} a bien été crée.", &house.name);
    house_list.lock().unwrap().insert(house.name.clone(), house);
}

fn list_house() {
    println!("Voici la liste des maisons.");
    let i = house_list.lock().unwrap();
    println!("{:?}", i.keys());
}

pub fn see_house() -> Option<String> {
    println!("Entrez le nom de la maison.");
    let name:String = read!("{}\n");

    let a = house_list.lock().unwrap();

    if a.contains_key(&name) {
        let i = &a.get(&name).unwrap();
        
        println!("La maison {} est située à {}, elle vaut {}, voici sa description : {}", &i.name, &i.location, &i.price, &i.description);
        return Some(i.name.clone())
    }
    println!("Cette maison n'existe pas");
    None
}

pub fn save_houses() {
    let i = house_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/houses.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/houses.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_houses() {
    // Create a path to the desired file
    let path = Path::new("./heart/data/houses.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, House> = serde_json::from_str(&s).unwrap();

            for (name, house) in &map {
                house_list.lock().unwrap().insert(name.clone(), house.clone());
            }
        },
    }
}