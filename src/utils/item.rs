extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::str::FromStr;
use std::sync::Mutex;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ItemType {
    Arme,
    Casque,
    Plastron,
    Pantalon,
    Bottes,
    Autres,
    Undefined,
}

impl FromStr for ItemType {
    type Err = ();

    fn from_str(s: &str) -> Result<ItemType, ()> {
        match s {
            "Arme" => Ok(ItemType::Arme),
            "Casque" => Ok(ItemType::Casque),
            "Plastron" => Ok(ItemType::Plastron),
            "Pantalon" => Ok(ItemType::Pantalon),
            "Bottes" => Ok(ItemType::Bottes),
            "Autres" => Ok(ItemType::Autres),
            _ => Ok(ItemType::Undefined),
        }
    }
}

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Item {
    pub name: String,
    pub item_type: ItemType,
    pub description: String,
}

impl Item {
    fn new_item(name:String, item_type:String, description:String) -> Item {
        Item {
            name:name,
            item_type:ItemType::from_str(&item_type).unwrap(),
            description:description
        }
    }
}

lazy_static! {
    static ref item_list: Mutex<HashMap<String, Item>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn item() {
    println!("==================");
    println!("1>Créer un item\n2>Voir la liste des items\n3>Voir un item\n4>Sauvegarder les items\n5>Retour en arrière");

    let cat:i32 = read!();

    if cat == 1 {
        create_item();
    } else if cat == 2 {
        list_items();
    } else if cat == 3 {
        println!("Entrez le nom de l'item désiré.");
        see_item();
    } else if cat == 4 {
        save_items();
    } else if cat == 0 {
        load_items();
    }
}

fn create_item() {
    println!("Entrez le nom de l'item");
    let name:String = read!("{}\n");

    println!("Entrez le type de l'item: Arme, Casque, Plastron, Pantalon, Bottes, Autres");
    let itype:String = read!();

    println!("Entrez la description de l'item.");
    let desc:String = read!("{}\n");
    let item:Item = Item::new_item(name,itype,desc);
    
    println!("L'Item : {} a bien été crée.", &item.name);
    item_list.lock().unwrap().insert(item.name.clone(), item);
}

fn list_items() {
    println!("Voici la liste des items");
    let i = item_list.lock().unwrap();
    println!("{:?}", i.keys());
}

pub fn see_item() -> Option<String> {
    let name:String = read!("{}\n");

    let i = item_list.lock().unwrap();
    if i.contains_key(&name) {
        let item = &i.get(&name).unwrap();
        println!("L'Item {} est un(e) {:?}, {}",&item.name, &item.item_type, &item.description);
        return Some(item.name.clone());
    }
    println!("L'Item n'existe pas.");
    None
}

pub fn save_items() {
    let i = item_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/items.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/items.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_items() {
    // Create a path to the desired file
    let path = Path::new("./heart/data/items.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Item> = serde_json::from_str(&s).unwrap();

            for (name, item) in &map {
                item_list.lock().unwrap().insert(name.clone(), item.clone());
            }
        },
    }
}