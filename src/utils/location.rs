extern crate serde;
extern crate serde_json;

use std::collections::HashMap;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::sync::Mutex;

#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Location {
    name: String,
    pallier: u64,
    x: f64,
    y: f64,
    z: f64,
    description: String,
}

impl Location {
    fn new_loc(name: String, pallier:u64, x:f64, y:f64, z:f64, description:String) -> Location {
        Location {
            name,
            pallier,
            x,
            y,
            z,
            description
        }
    }
}

lazy_static! {
    static ref loc_list: Mutex<HashMap<String, Location>> = 
        Mutex::new(HashMap::new()); // keys represent dimensions of matrices
}

pub fn location() {
    println!("==================");
    println!("1>Créer une Location\n2>Voir la liste des Locations\n3>Voir une Location \n4>Sauvegarder les Locations\n5>Retour en arrière");

    let cat:i32 = read!();

    if cat == 1 {
        create_loc();
    } else if cat == 2 {
        list_loc();
    } else if cat == 3 {
        see_loc();
    } else if cat == 4 {
        save_locations();
    }
}

fn create_loc() {
    println!("Entrez le nom de la Location.");
    let name:String = read!("{}\n");

    println!("Entrez le numéro du Pallier");
    let pallier:u64 = read!("{}\n");

    println!("Entrez les coordonnées X");
    let x:f64 = read!("{}\n");

    println!("Entrez les coordonnées Y");
    let y:f64 = read!("{}\n");

    println!("Entrez les coordonnées Z");
    let z:f64 = read!("{}\n");

    println!("Entrez la description du lieu.");
    let description:String = read!("{}\n");

    let  location = Location::new_loc(name,pallier,x,y,z,description);

    println!("La location {} a bien été crée.", &location.name);
    loc_list.lock().unwrap().insert(location.name.clone(), location);
}

fn list_loc() {
    println!("Voici la liste des locations");
    let i = loc_list.lock().unwrap();
    println!("{:?}", i.keys());
}

pub fn see_loc() -> Option<String> {
    println!("Entrez le nom de la location.");
    let name:String = read!("{}\n");

    let a = loc_list.lock().unwrap();
    
    if a.contains_key(&name) {
        let i = &a.get(&name).unwrap();

        println!("{} est situé au pallier : {}, aux coordonnées {} {} {}, description : {}",&i.name, &i.pallier, &i.x, &i.y ,&i.z, &i.description);
        return Some(i.name.clone())
    }
    println!("La location n'existe pas.");
    None
}

pub fn save_locations() {
    let i = loc_list.lock().unwrap();
    let json = serde_json::to_string_pretty(&*i).unwrap();

    let path = Path::new("./heart/data/locations.json");
    let display = path.display();

    let mut file = match File::create("./heart/data/locations.json") {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all(json.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn load_locations() {
    // Create a path to the desired file
    let path = Path::new("./heart/data/locations.json");
    let display = path.display();

    if !path.exists() {
        let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    match file.write_all("{}".as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
    }

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => {
            let map:HashMap<String, Location> = serde_json::from_str(&s).unwrap();

            for (name, location) in &map {
                loc_list.lock().unwrap().insert(name.clone(), location.clone());
            }
        },
    }
}