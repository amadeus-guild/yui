#[derive(Serialize, Deserialize,Clone, Debug)]
pub struct Stats {
    pub health:u32,
    pub attack:u32,
    pub defence:u32,
    pub speed:u32,
}

impl Stats {
    pub fn new_stats(health: u32, attack: u32, defence: u32, speed: u32) -> Stats {
        Stats {
            health,
            attack,
            defence,
            speed,
        }
    }
}

pub fn create_stats() -> Stats {
    println!("Entrez la Vie.");
    let health:u32 = read!("{}\n");

    println!("Entrez l'attaque");
    let attack:u32 = read!("{}\n");

    println!("Entrez la Defense");
    let defence:u32 = read!("{}\n");

    println!("Entrez la vitesse");
    let speed:u32 = read!("{}\n");

    Stats::new_stats(health, attack, defence, speed)
}